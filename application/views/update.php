<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<html>
	<body></body>
	<script>
		//00 degrees 13 minutes south, 13 degrees 58 minutes east
		var clues = [];
		$(document).ready(function(){
			$.get( "/welcome/get_page/4", function( data ) {
				var html = $.parseHTML(data);
			  	var rows = $(html).find(".wikitable tbody tr");
			  	$.each( rows, function( row_index, value ){
			  		var tds = $(value).find("td");
			  		var clue = {};
			  		var answer = "";
				  	$.each( tds, function( index, td ){
				  		if($(td).text().indexOf("<img") < 0){
				  			if(index == 2){
				  				clue.clue_answer = "Requirements: " + $(td).text() + "\n";
				  				clue.clue_answer += $($(rows[row_index+4]).children("td")[0]).text();
				  				clue.img = $($(rows[row_index+2]).children("td")[0]).children("a").attr('href');
					  		}else if(index==0){
					  			clue.clue_text = $(td).text();
					  			var arr = clue.clue_text.replace(", ", ",").split(" ");
					  			clue.y_1 = arr[0];
					  			clue.y_2 = arr[2];
					  			if(typeof arr[4] !== "undefined"){
						  			var arr2 = arr[4].split(",\n");
						  			if(typeof arr2[0] !== "undefined")
						  				clue.y_direction = arr2[0].toUpperCase().charAt(0);
						  			if(typeof arr[8] !== "undefined")
						  				clue.x_direction = arr[8].toUpperCase().charAt(0);
						  			clue.x_1 = arr2[1];
						  			clue.x_2 = arr[6];
						  		}
					  		}else if(index==4){
				  				if($(this).text() == "\n" || $(this).text() == "{{{level}}}\n")
				  					clue.level = 0;
				  				else
				  					clue.level = $(this).text().replace(/(\r\n|\n|\r)/gm,"");
					  		}
				  		}
					});
					if(!jQuery.isEmptyObject(clue)){
						clue.type = "4";
						if(clue.hasOwnProperty("clue_answer")){
					  		clues.push(clue);
					  	}
					}
				});
				var clues_cp = clues;
				$.ajax({
				  	method: "POST",
				  	url: "/welcome/do_update",
				  	data: { clues: clues.slice(50,101) }
				}).done(function( msg ) {
				    document.write( msg );
					$.ajax({
					  	method: "POST",
					  	url: "/welcome/do_update",
					  	data: { clues: clues_cp.slice(0,50) }
					}).done(function( msg ) {
					    document.write( msg );
					});
				});
			});
		}); 

		Anagrams
		var clues = [];
		$(document).ready(function(){
			$.get( "/welcome/get_page/3", function( data ) {
				var html = $.parseHTML(data);
			  	var rows = $(html).find(".wikitable tbody tr");
			  	$.each( rows, function( index, value ){
			  		var tds = $(value).find("td");
			  		var clue = {};
			  		var answer = "";
				  	$.each( tds, function( index, td ){
				  		//if(index != 1){
				  			if(index == 1 || index == 2 || index == 3){
					  			if(index == 3){
					  				answer += " - Challenge Answer is: ";
					  				answer += $(td).text()
					  				clue.clue_answer = answer;
					  				answer = "";
					  			}else if(index == 1){
					  				answer += $(td).text();
					  				answer += " - ";
					  			}else{
					  				answer += $(td).text();
					  			}
					  		}else if(index==0){
					  			clue.clue_text = $(td).text();
					  		}else if(index==4){
				  				if($(this).text() == "\n")
				  					clue.level = 0;
				  				else
				  					clue.level = $(this).text().replace(/(\r\n|\n|\r)/gm,"");
					  		}
					  		clue.level = 0;
				  			clue.img = "null";
				  		//}
					});
					if(!jQuery.isEmptyObject(clue)){
						clue.type = "3";
				  		clues.push(clue);
					}
				});
				$.ajax({
				  	method: "POST",
				  	url: "/welcome/do_update",
				  	data: { clues: clues }
				}).done(function( msg ) {
				    document.write( msg );
				});
			});
		});
	</script>

	<script>
	/* Challenge clues */
		var clues = [];
		$(document).ready(function(){
			$.get( "/welcome/get_page/5", function( data ) {
				var html = $.parseHTML(data);
			  	var rows = $(html).find(".wikitable tbody tr");
			  	$.each( rows, function( index, value ){
			  		var tds = $(value).find("td");
			  		var clue = {};
				  	$.each( tds, function( index, td ){
				  		//if(index != 1){
				  			if(index==2){
					  			clue.clue_answer = $(td).html();
					  		}else if(index==1){
					  			clue.clue_text = $(td).text();
					  		}
					  		clue.level = 0;
				  			clue.img = "null";
				  		//}
					});
					if(!jQuery.isEmptyObject(clue)){
						clue.type = "5";
				  		clues.push(clue);
					}
				});
				$.ajax({
				  	method: "POST",
				  	url: "/welcome/do_update",
				  	data: { clues: clues }
				}).done(function( msg ) {
				    document.write( msg );
				});
			});
		});
	</script>

	<script>
		var clues = [];
		$(document).ready(function(){
			$.get( "/welcome/get_page/1", function( data ) {
				var html = $.parseHTML(data);
			  	var rows = $(html).find(".wikitable tbody tr");
			  	$.each( rows, function( index, value ){
			  		var tds = $(value).find("td");
			  		var clue = {};
				  	$.each( tds, function( index, td ){
				  		//if(index != 1){
				  			if($(td).find("a").length > 0 && index == 3){
				  				clue.img = $(td).find("a").attr('href');
				  			}else if(index == 4){
				  				if($(this).text() == "\n")
				  					clue.level = 0;
				  				else
				  					clue.level = $(this).text().replace(/(\r\n|\n|\r)/gm,"");
				  			}else if(index==0){
					  			clue.clue_text = $(td).text();
					  		}else if(index==1){
					  			clue.clue_answer = $(td).html();
					  		}
				  		//}
					});
					if(!jQuery.isEmptyObject(clue)){
						clue.type = "1";
				  		clues.push(clue);
					}
				});
				$.ajax({
				  	method: "POST",
				  	url: "/welcome/do_update",
				  	data: { clues: clues }
				}).done(function( msg ) {
				    document.write( msg );
				});
			});
		});


	</script>

	<script>
	/***************  Cryptic clues *************/

		var clues = [];
		$(document).ready(function(){
			$.get( "/welcome/get_page/2", function( data ) {
				var html = $.parseHTML(data);
			  	var rows = $(html).find(".wikitable tbody tr");
			  	$.each( rows, function( index, value ){
			  		var tds = $(value).find("td");
			  		var clue = {};
				  	$.each( tds, function( index, td ){
				  		if(index != 2){
				  			if($(td).find("a").length > 0 && index == 3){
				  				clue.img = $(td).find("a").attr('href');
				  			}else if(index == 4){
				  				if($(this).text() == "\n")
				  					clue.level = 0;
				  				else
				  					clue.level = $(this).text().replace(/(\r\n|\n|\r)/gm,"");
				  			}else if(index==0){
					  			clue.clue_text = $(td).text();
					  		}else if(index==1){
					  			clue.clue_answer = $(td).text();
					  		}
				  		}
					});
					if(!jQuery.isEmptyObject(clue)){
						clue.type = "2";
				  		clues.push(clue);
					}
				});
				$.ajax({
				  	method: "POST",
				  	url: "/welcome/do_update",
				  	data: { clues: clues }
				}).done(function( msg ) {
				    document.write( msg );
				});
			});
		});

	</script>
</html>