<?php include('includes/header.php');?>

<div class="container">
	<div class="row">
	  	<div class="col-sm-4">
	  		<div class="pull-right" style="height:100%;display:inline-flex;">
	  			<a href="#" class="btn btn-danger btn-lg"  style="margin: auto 5px;font-size: 5em;"><i class="fa fa-thumbs-down"></i></a>
	  		</div>
	  	</div>
	  	<div class="col-sm-4">
		  	<div class="text-center">
		    	<h1 id="user_name"></h1>
		    	<img id="user_avatar" style="min-width:250px;" src=""/><br>
		    	<img id="user_country" class="flag"/>
		  	</div>
	  	</div>
	  	<div class="col-sm-4">
	  		<div class="pull-left" style="height:100%;display:inline-flex;">
	  			<a href="#" class="btn btn-success btn-lg"  style="margin: auto 5px;font-size: 5em;"><i class="fa fa-thumbs-up"></i></a>
	  		</div>
	  	</div>
	</div>
	<div class="row">
		<div class="text-center col-sm-12">
			<hr>
			<h1>Rank</h1>
			<img src="http://csgoteamfinder.com/img/ranks/elo7.png"/>
			<hr>
			<h1>Stats</h1><br>
			<table class="table" style="text-align:center;">
				<thead>
					<td>K/D Ratio</td>
					<td>Kills Per round</td>
					<td>Headshot %</td>
					<td>Win %</td>
					<td>Best Map</td>
					<td>Best Gun</td>
				</thead>
				<tr>
					<td>1.03</td>
					<td>0.5</td>
					<td>32%</td>
					<td>46%</td>
					<td>Dust II</td>
					<td>AK-47</td>
				</tr>
			</table>
		</div>
	</div>
</div>
	
<?php include('includes/footer.php');?>