<div class="container-fluid">
	<div class="col-xs-12">
		<h1><?php echo $type_text;?></h1><br>
		<h4><b>Clue:</b><h4>
		<p><?php echo $clue_text; ?></p>
		<h4><b>Solution:</b><h4>
		<p><?php echo $clue_answer_text; ?></p>
		<h4><b>Image:</b><h4>
		<p><img src="<?php echo $clue_img; ?>"/></p>
		<h4><b>Map:</b></h4>
		<iframe class="fit" src="https://www.osrsmap.com/#clue/<?php echo $y_1;?>-<?php echo $y_2;?>-<?php echo $y_direction;?>/<?php echo $x_1;?>-<?php echo $x_2;?>-<?php echo $x_direction;?>/5/"></iframe>
		<p>Credit to <a href="https://www.osrsmap.com/">OSRSMap</a> for the maps</p><br>
		<h4><b>Difficulty:</b><h4>
		<p><?php echo $level_text; ?></p>
	</div>
</div>