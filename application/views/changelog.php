<?php $this->load->view('includes/header', array('active_page' => 'Changelog'));?>
<div class="container component_contianer changelog">
    <div class="col-xs-12 col-md-8 col-md-offset-2 search_container">
        <img class="widescroll-top" src="/assets/img/backdrop_765_top.gif">
        <h1>RsClues - Changelog</h1>
        <h3>09/06/2015</h3>
        <div class="container-fluid">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                <ul>
                    <li>- Moved Search again bar to the top of each clue <a href="http://www.reddit.com/r/2007scape/comments/396x6o/rsclues_latest_version_update/cs0unc7">Suggestion</a></li>
                    <li>- Maps added for cryptic clues (thanks to <a href="https://www.osrsmap.com/">OSRSMap</a>)</li>
                    <li>- Maps added for emote clues (thanks to <a href="https://www.osrsmap.com/">OSRSMap</a>)</li>
                    <li>- Maps added for cryptic clues (thanks to <a href="https://www.osrsmap.com/">OSRSMap</a>)</li>
                    <li>- Added a search bar to each clue page</li>
                    <li>- Removed Difficulty Option (It was just clutter)</li>
                    <li>- Added a link to a puzzle solver on the homepage</li>
                    <li>- Added a link to map clues on the homepage</li>
                </ul>
            </div>
        </div>
        <h3>29/05/2015</h3>
        <div class="container-fluid">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                <ul>
                    <li>- Coordinate Clues added</li>
                    <li>- Latest Update added to the homepage</li>
                    <li>- Maps added to coordinate clues (thanks to <a href="https://www.osrsmap.com/">OSRSMap</a>)</li>
                </ul>
            </div>
        </div>
        <h3>28/05/2015</h3>
        <div class="container-fluid">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                <ul>
                    <li>- Changelog added</li>
                </ul>
            </div>
        </div>
        <h3>26/05/2015</h3>
        <div class="container-fluid">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                <ul>
                    <li>- Initial release of website</li>
                    <li>- Minor bug fixes</li>
                </ul>
            </div>
        </div>



        <a id="home_button" class="btn btn-default btn-osrs" href="/">Back to homepage</a>
        <span class="links">Thanks to the <a href="http://2007.runescape.wikia.com/wiki/2007scape_Wiki">OSRS wikia</a> for the clue scroll data</span>
        <img class="widescroll-bottom" src="/assets/img/backdrop_765_top.gif">
    <div>
</div>
<?php $this->load->view('includes/footer');?>