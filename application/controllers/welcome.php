<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{	
		$update = false;

		if(!$update)
			$this->load->view('home');
		else
			$this->load->view('update2');

	}

	public function about()
	{
		$this->load->view('about');
	}

	public function changelog()
	{
		$this->load->view('changelog');
	}

	public function get_page($page)
	{
		if($page == 3)
			$url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Anagrams";
		if($page == 2)
			$url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Cryptics";
		if($page == 1)
			$url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Emotes";
		if($page == 4)
			$url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Coordinates";
		if($page == 5)
			$url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Challenge_scrolls";

		echo file_get_contents($url);
	}

	public function update()
	{

		$this->load->view('update');
	}

	public function do_update()
	{
		$this->load->database();
		$filter = array("\n", "'");
		echo count($_POST['clues']);
		if($_POST){
			$array = $_POST['clues'];
			foreach($array as $clue){
				if(!isset($clue['img'])){
					$clue['img'] = "";
				}
				if(!isset($clue['level'])){
					$clue['level'] = "0";
				}

				if(!$clue['level']){
					$clue['level'] = "0";
				}

				$clue['type'] = 4;

				//$qry = "INSERT INTO clues VALUES ('', ".$clue['type'].",'".str_replace($filter, "", $clue['clue_text'])."',".$clue['level'].",'".str_replace($filter, "", $clue['clue_answer'])."', '".$clue['img']."');";
				$qry = "INSERT INTO clues VALUES ('', ".$clue['type'].",'".str_replace($filter, "", $clue['clue_text'])."',".$clue['level'].",'".str_replace($filter, "", $clue['clue_answer'])."', '".$clue['img']."', ".$clue['y_1'].", ".$clue['y_2'].", '".$clue['y_direction']."', ".$clue['x_1'].", ".$clue['x_2'].", '".$clue['x_direction']."');";
				//echo $qry."<br><br>";
				//$this->db->query($qry);
				
			}
		}
	}

	public function get_clues()
	{
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
		header('Content-Type: application/json');

		$q = "";

		if(isset($_GET) && $_GET){
			$q = str_replace("'", "", urldecode($_GET['q']));
		}

		$this->load->database();
		if($q)
			$query = $this->db->query("SELECT clues.id as data, clues.clue_text as value FROM clues WHERE clues.clue_text LIKE '%".$q."%'");
		else
			$query = $this->db->query("SELECT clues.id as data, clues.clue_text as value FROM clues");

		

		$output = '[';

		$x = 0;
		foreach($query->result_array() as $item){
			if($x != 0){
				$output .=  ",";
			}
			$x++;
			$output .= '{ "id" : "'.$item['data'].'", "value" : "'.$item['value'].'" }';
		}
		$output .= ']';

		if($x == 0){
			$output = '["No results were found"]';
		}

		echo $output;
	}

	public function error_404()
	{
		echo '404';
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */